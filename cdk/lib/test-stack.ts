import {App, Duration, Stack, StackProps} from 'aws-cdk-lib';
import {SubnetType, Vpc} from 'aws-cdk-lib/aws-ec2';
import {RetentionDays} from 'aws-cdk-lib/aws-logs';
import {Construct} from 'constructs';
import {LambdaTest} from './lambda-test/lambda-test';
import {getVpc} from "@agregio/cdk-utils/lib/vpc/vpc-tools";

export class TestStack extends Stack {
    constructor(scope: App, id: string, props?: StackProps) {
        super(scope, id, props);

        const vpc = getVpc(this);
        // Create dispatch database and user on RDS instance.
        new LambdaTest(this, 'lambda-test-jbarbe', {
            config: {},
            fnLogRetention: RetentionDays.ONE_MONTH,
            fnCodePath: `${__dirname}/function`,
            fnTimeout: Duration.minutes(2),
            fnSecurityGroups: [],
            vpc,
            subnetsSelection: vpc.selectSubnets({
                subnetType: SubnetType.PRIVATE_WITH_NAT
            })
        });
    }
}
