// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

import {Duration, Stack} from "aws-cdk-lib"
import {ISecurityGroup, IVpc, SecurityGroup, SubnetSelection} from "aws-cdk-lib/aws-ec2"
import {Code, Function, Runtime} from "aws-cdk-lib/aws-lambda"
import {RetentionDays} from "aws-cdk-lib/aws-logs"
import {Construct} from "constructs"

export interface FunctionProps {
    vpc: IVpc
    subnetsSelection: SubnetSelection
    fnSecurityGroups: ISecurityGroup[]
    fnTimeout: Duration
    fnCodePath: string
    fnLogRetention: RetentionDays
    fnMemorySize?: number
    config: any
}

export class LambdaTest extends Construct {
    constructor(scope: Construct, id: string, props: FunctionProps) {
        super(scope, id)

        const stack = Stack.of(this)

        const fnName = 'DatabaseInitializerFn';
        const fnSg = new SecurityGroup(this, `${fnName}Sg`, {
            securityGroupName: `${id}${fnName}Sg`,
            vpc: props.vpc,
            allowAllOutbound: true
        })

        new Function(this, 'DatabaseInitializerFct', {
            memorySize: props.fnMemorySize || 128,
            functionName: `${id}-ResInit${stack.stackName}`,
            code: Code.fromAsset(props.fnCodePath),
            vpcSubnets: props.vpc.selectSubnets(props.subnetsSelection),
            vpc: props.vpc,
            securityGroups: [fnSg, ...props.fnSecurityGroups],
            timeout: props.fnTimeout,
            logRetention: props.fnLogRetention,
            allowAllOutbound: true,
            handler: 'index.handler',
            runtime: Runtime.NODEJS_14_X
        });
    }
}
