// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

exports.handler = async (e) => {
    try {
        const {config} = e.params
        console.log(`Starting lambda test with config ${JSON.stringify(config)}`);
        return {
            status: 'OK'
        }
    } catch (err) {
        return {
            status: 'ERROR',
            err,
            message: err.message
        }
    }
}
