#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import {Stack, Tags} from 'aws-cdk-lib';
import {TestStack} from "../lib/test-stack";

const app = new cdk.App();

const env = { account: process.env.CDK_DEPLOY_ACCOUNT, region: process.env.CDK_DEPLOY_REGION }

const lambdaTestStack = new TestStack(app, 'test-stack-jbebar', { env });
addTags(lambdaTestStack);

app.synth();

function addTags(stack: Stack) {
  Tags.of(stack).add('Team', 'vpp');
  Tags.of(stack).add('Domain', 'dispatch');
  Tags.of(stack).add('SubDomain', 'dispatch');
  Tags.of(stack).add('Comp', "dispatch");
  Tags.of(stack).add('InfraAsCode', 'cdk');
  Tags.of(stack).add('OffHoursPolicy', 'stopped');
}
